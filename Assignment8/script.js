$("#items li").click(function () {
    var itemNumber = $(this).prevAll().length;
    var l = (90 * (itemNumber - 1)) + 20;
    var t = (53 * (itemNumber - 1)) + 40;
    var duration = (100 * (itemNumber - 1)) + 1000;
    $(".bullet").animate({
        top: t + "px",
        left: l + "px"
    }, duration, function () {
        window.location.replace("https://www.youtube.com/");
    })
});
var menuHidden = true;
$("#menu").click(function () {
    if(menuHidden){
        showMenu();
        menuHidden = false;
    }else{
        hideMenu();
        menuHidden = true;
    }
});
function showMenu(){
    $(".bullet").css("display", "inline-block");
    $(".bullet").animate({
        left: "-8px",
        top: "4px"
    }, 1000);
    $(".rotatingDiv").css("transform", "rotate(-30deg)");
    $("#items li").css({ "display": "inline-block" });

    for (var i = 1; i <= 5; i++) {
        $("#" + i).animate({
            left: (i * 105) + "px"
        }, 1500)
    }
}
function hideMenu(){
    $(".bullet").animate({
        left: "-100px",
        top: "-8px"
    }, 1000,()=>{
        $(".bullet").css("display", "none");
    });
    
    $(".rotatingDiv").css("transform", "rotate(0deg)");
    for (var i = 1; i <= 5; i++) {
        $("#" + i).animate({
            left: (0) + "px"
        }, 1500,()=>{
            $("#items li").css({ "display": "none" });
        })
    }
    
}