DROP DATABASE Pokedex;
CREATE DATABASE Pokedex;
USE Pokedex;

CREATE TABLE Pokemon(NatNo int,Name VARCHAR(50),Type1 VARCHAR(50),Type2 VARCHAR(50),HP int,Atk int,Def int,SAt int,SDf int,Spd int,BST int);
INSERT INTO Pokemon VALUES(387,"Turtwing","GRASS",Null,55,68,64,45,44,31,318);
INSERT INTO Pokemon VALUES(388,"Grotle","GRASS",Null,75,89,85,55,65,36,405);
INSERT INTO Pokemon VALUES(389,"Torterra","GRASS","GROUND",95,109,105,75,85,56,525);
INSERT INTO Pokemon VALUES(390,"Chimchar","FIRE",Null,44,58,44,58,44,61,309);
INSERT INTO Pokemon VALUES(391,"Monferno","FIRE","FIGHT",64,78,52,78,52,81,405);
INSERT INTO Pokemon VALUES(392,"Infernape","FIRE","FIGHT",76,104,71,104,71,108,534);
INSERT INTO Pokemon VALUES(393,"Piplup","WATER",Null,53,51,53,61,56,40,314);
INSERT INTO Pokemon VALUES(394,"Prinplup","WATER",Null,64,66,68,81,76,50,405);
INSERT INTO Pokemon VALUES(395,"Empoleon","WATER","STEEL",84,86,88,111,101,60,530);
INSERT INTO Pokemon VALUES(396,"Starly","NORMAL","FLYING",40,55,30,30,30,60,245);
INSERT INTO Pokemon VALUES(397,"Staravia","NORMAL","FLYING",55,75,50,40,40,80,340);
INSERT INTO Pokemon VALUES(398,"Staraptor","NORMAL","FLYING",85,120,70,50,60,100,485);
INSERT INTO Pokemon VALUES(399,"Bidoof","NORMAL",Null,59,45,40,35,40,31,250);
INSERT INTO Pokemon VALUES(400,"Bibarel","NORMAL","WATER",79,85,60,55,60,71,410);
INSERT INTO Pokemon VALUES(401,"Kricketot","BUG",Null,37,25,41,25,41,25,194);
INSERT INTO Pokemon VALUES(402,"Kricketune","BUG",Null,77,85,51,55,51,65,384);
INSERT INTO Pokemon VALUES(402,"Kricketune","BUG",Null,77,85,51,55,51,65,384);
INSERT INTO Pokemon VALUES(403,"Shinx","ELECTR",Null,45,65,34,40,34,45,263);
INSERT INTO Pokemon VALUES(404,"Luxio","ELECTR",Null,60,85,49,60,49,60,363);
INSERT INTO Pokemon VALUES(405,"Luxray","ELECTR",Null,80,120,79,95,79,70,523);
INSERT INTO Pokemon VALUES(406,"Budew","GRASS","POISON",40,30,35,50,70,55,280);
INSERT INTO Pokemon VALUES(407,"Roserade","GRASS","POISON",60,70,65,125,105,90,515);
INSERT INTO Pokemon VALUES(408,"Cranidos","ROCK",Null,67,125,40,30,30,58,350);
INSERT INTO Pokemon VALUES(409,"Rampardos","ROCK",Null,97,165,60,65,50,58,495);
INSERT INTO Pokemon VALUES(410,"Shieldon","ROCK","STEEL",30,42,118,42,88,30,350);


CREATE TABLE Effectiveness(Type VARCHAR(50),StrongAgainst VARCHAR(50),WeakAgainst VARCHAR(50),ResistantTo VARCHAR(50),VulerableTo VARCHAR(50));
INSERT INTO Effectiveness VALUES("Normal",Null,"Rock, Ghost, Steel","Ghost","Fighting");
INSERT INTO Effectiveness VALUES("Fight","Normal, Rock, Steel, Ice, Dark","Flying, Poison, Psychic, Bug, Ghost, Fairy","Rock, Bug, Dark","Flying, Psychic, Fairy");
INSERT INTO Effectiveness VALUES("Flying","Fighting, Bug, Grass","Rock, Steel, Electric","Fighting, Ground, Bug, Grass","Rock, Electric, Ice");
INSERT INTO Effectiveness VALUES("Ground","Poison, Rock, Steel, Fire, Electric","Flying, Bug, Grass,","Poison, Rock, Electric","Water, Grass, Ice");
INSERT INTO Effectiveness VALUES("Rock","Flying, Bug, Fire, Ice","Fighting, Ground, Steel","Normal, Flying, Poison, Fire","Fighting, Ground, Steel, Water, Grass");
INSERT INTO Effectiveness VALUES("Bug","Grass, Psychic, Dark","Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy","Fighting, Ground, Grass","Flying, Rock, Fire");
INSERT INTO Effectiveness VALUES("Ghost","Ghost, Psychic","Normal, Dark","Normal, Fighting, Poison, Bug","Ghost, Dark");
INSERT INTO Effectiveness VALUES("Steel","Rock, Ice, Fairy","Steel, Fire, Water, Electric","Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy","Fighting, Ground, Fire");
INSERT INTO Effectiveness VALUES("Fire","Bug, Steel, Grass, Ice","Rock, Fire, Water, Dragon","Bug, Steel, Fire, Grass, Ice","Ground, Rock, Water");
INSERT INTO Effectiveness VALUES("Water","Ground, Rock, Fire","Water, Grass, Dragon","Steel, Fire, Water, Ice","Grass, Electric");
INSERT INTO Effectiveness VALUES("Grass","Ground, Rock, Water","Flying, Poison, Bug, Steel, Fire, Grass, Dragon","Ground, Water, Grass, Electric","Flying, Poison, Bug, Fire, Ice");
INSERT INTO Effectiveness VALUES("Electric","Flying, Water","Ground, Grass, Electric, Dragon","Flying, Steel, Electric","Ground");
INSERT INTO Effectiveness VALUES("Psychic","Fighting, Poison","Steel, Psychic, Dark","Fighting, Psychic","Bug, Ghost, Dark");
INSERT INTO Effectiveness VALUES("Ice","Flying, Ground, Grass, Dragon","Steel, Fire, Water, Ice","Ice","Fighting, Rock, Steel, Fire");
INSERT INTO Effectiveness VALUES("Dragon","Dragon","Steel, Fairy","Fire, Water, Grass, Electric","Ice, Dragon, Fairy");
INSERT INTO Effectiveness VALUES("Fairy","Fighting, Dragon, Dark","Poison, Steel, Fire","Fighting, Bug, Dragon, Dark","Poison, Steel");
INSERT INTO Effectiveness VALUES("Dark","Ghost, Psychic","Fighting, Dark, Fairy","Ghost, Psychic, Dark","Fighting, Bug, Fairy");

CREATE TABLE FavouritePokemons(NatNo int,Name VARCHAR(50),Type1 VARCHAR(50),Type2 VARCHAR(50),HP int,Atk int,Def int,SAt int,SDf int,Spd int,BST int);
INSERT INTO FavouritePokemons VALUES(398,"Staraptor","NORMAL","FLYING",85,120,70,50,60,100,485);
INSERT INTO FavouritePokemons VALUES(399,"Bidoof","NORMAL",Null,59,45,40,35,40,31,250);
INSERT INTO FavouritePokemons VALUES(400,"Bibarel","NORMAL","WATER",79,85,60,55,60,71,410);
INSERT INTO FavouritePokemons VALUES(401,"Kricketot","BUG",Null,37,25,41,25,41,25,194);
INSERT INTO FavouritePokemons VALUES(402,"Kricketune","BUG",Null,77,85,51,55,51,65,384);


DELIMITER $$
CREATE PROCEDURE getTypes()
BEGIN
    SELECT Type from Effectiveness;
END$$


DELIMITER $$
CREATE PROCEDURE getPokemon(type VARCHAR(20))
BEGIN
  SELECT NatNo,Name,Type1,Type2 from Pokemon where Type1=type or Type2=type;
END$$

DELIMITER $$
CREATE PROCEDURE getAllPokemon()
BEGIN
  SELECT NatNo,Name,Type1,Type2 from Pokemon;
END$$
