<?php
// ***************************************************

  $servername = "localhost";
  $username = "root";
  $password = "";
  $database = "Pokedex";

  $conn = mysqli_connect($servername,$username,$password,$database);
  if (!$conn) {
        die("Connection failed: " . mysqli_connect_error());
    }

// ***********************************************************************
      $bestFour = array();
      $favouriteSeven = array();
      $selectAll="SELECT * from Pokemon ORDER BY BST DESC;";
      if(mysqli_multi_query($conn, $selectAll)){
        do{
          if ($result = mysqli_use_result($conn)) {
            $n=0;
              while ($row = mysqli_fetch_row($result)) {
                  if($n<4){
                    $bestFour[$n]= $row[1];
                  }
                  if($n<7){
                    $favouriteSeven[$n]= array($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7],$row[8],$row[9],$row[10]);
                  }
                  $n++;
              }
            mysqli_free_result($result);
          }
        }while(mysqli_next_result($conn));
      }
// *************************************************************
    $favouritePokemons = array();
    $getFavourite = "SELECT * FROM FavouritePokemons;";
    if(mysqli_multi_query($conn, $getFavourite)){
      do{
        if ($result = mysqli_use_result($conn)) {
          $n=0;
            while ($row = mysqli_fetch_row($result)) {
                $favouritePokemons[$n] = array($row[0],$row[1],$row[2],$row[3],$row[4],$row[5],$row[6],$row[7],$row[8],$row[9],$row[10]);
                $n++;
            }
          mysqli_free_result($result);
        }
      }while(mysqli_next_result($conn));
    }

mysqli_close($conn);
//****************************************************************
require_once("vendor/autoload.php");
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

  echo $twig->render('header.html',array(
      "title" => "My Favourites"
  ));

  echo $twig->render('sidebar.html',array(
      "bestFour" => array($bestFour)
  ));

  echo $twig->render('cards.html',array(
      "pokemonData" => array($favouritePokemons)
  ));

  echo $twig->render('table.html',array(
      "pokemonData" => array($favouriteSeven)
  ));

  echo $twig->render('footer.html');

?>
