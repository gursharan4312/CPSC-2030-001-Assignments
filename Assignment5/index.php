<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Pokedex</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <?php
      $servername = "localhost";
      $username = "CPSC2030";
      $password = "CPSC2030";

      $conn = mysqli_connect($servername,$username,$password);
      if (!$conn) {
            die("Connection failed: " . mysqli_connect_error());
        }
  ?>
        <nav>
              <form action="index.php" method="POST">
                <?php
                    $databaseQuery="use Pokedex;";
                    $typeSelectQuery= "CALL getTypes()";
                    if(mysqli_multi_query($conn, $databaseQuery)){
                      if(mysqli_multi_query($conn, $typeSelectQuery)){
                        do{
                          if ($result = mysqli_use_result($conn)) {
                            $n=1;
                              while ($row = mysqli_fetch_row($result)) {
                                  echo "<input type='submit' name='",$n,"' value='",$row[0],"' />";
                                  $n++;
                              }
                            mysqli_free_result($result);
                          }
                        }while(mysqli_next_result($conn));
                      }
                    }
                      mysqli_close($conn);
                ?>
              </form>
        </nav>
        <main>

            <form method="get" action="details.php">
                <div class="pokemonContainer">
          <?php

               if($_SERVER['REQUEST_METHOD']=="POST"){
                 for ($i=1; $i <=$n ; $i++) {
                   if(isset($_POST[$i])){
                     displayThisType($_POST[$i]);
                   }
                 }
               }else{
                 displayThisType("");
               }
               function displayThisType($type){
                 $conn = mysqli_connect( "localhost","CPSC2030","CPSC2030");
                 $databaseQuery="use Pokedex;";
                 if($type !== ""){
                   $selectPokemonQuery = "CALL getPokemon('$type')";
                 }else{
                   $selectPokemonQuery = "CALL getAllPokemon()";
                 }

                    if(mysqli_multi_query($conn, $databaseQuery)){
                      if(mysqli_multi_query($conn, $selectPokemonQuery)){
                        do{
                          if ($result = mysqli_use_result($conn)) {
                              while ($row = mysqli_fetch_row($result)) {
                                  createCard($row);
                              }
                              mysqli_free_result($result);
                          }
                        }while(mysqli_next_result($conn));
                      }
                    }
                    mysqli_close($conn);
               }
               function createCard($data){
                  echo "<div class='pokemonCard'>",
                       "<h3>Pokemon Number: ",$data[0],"</h3>",
                       "<h3>Name: ",$data[1],"</h3>",
                       "<h4>Type: ",$data[2],",",$data[3],"</h4>",
                       "<button type='submit' name='pokemonNumber' value='",$data[0],"'>Details</button>",
                       "</div>";
               }
          ?>

        </div>
          </form>
        </main>
</body>
</html>
