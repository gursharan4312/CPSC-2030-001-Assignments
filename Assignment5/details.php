<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Pokedex</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="pokemonBody">
      <?php
      $conn = mysqli_connect( "localhost","CPSC2030","CPSC2030");
      $databaseQuery="use Pokedex;";
      $NatNo = $_GET["pokemonNumber"];
      $pokemonDetailsQuery = "CALL getAllPokemonDetail($NatNo)";
      if(mysqli_multi_query($conn, $databaseQuery)){
        if(mysqli_multi_query($conn, $pokemonDetailsQuery)){
          do{
            if ($result = mysqli_use_result($conn)) {
                while ($row = mysqli_fetch_row($result)) {
                    showDetails($row);
                }
                mysqli_free_result($result);
            }
          }while(mysqli_next_result($conn));
        }
      }
      mysqli_close($conn);

      function showDetails($data){
        echo "<div class='pokemonDetail'>",
            "<h1>Name: ",$data[1],"</h1>",
             "<h3>Pokemon Number: ",$data[0],"</h3>",
             "<h4>Type: ",$data[2],",",$data[3],"</h4>",
             "HP: ",$data[4],"<br>",
             "Attack: ",$data[5],"<br>",
             "Defence: ",$data[6],"<br>",
             "Special Attack:",$data[7],"<br>",
             "Special Defence:",$data[8],"<br>",
             "Speed: ",$data[9],"<br>",
             "B.S.T: ",$data[10],"<br>",
             "<h4>Strong Against: </h4>",$data[12],
             "<h4>Weak Against: </h4>",$data[13],
             "<h4>Resistant to: </h4>",$data[14],
             "<h4>Vulerable to : </h4>",$data[15],"<br>","<br>",
             "<button><a href='index.php'>back</a></button>",
             "</div>";
      }
      ?>
  </div>
</body>
</html>
