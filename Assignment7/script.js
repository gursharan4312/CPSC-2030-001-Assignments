// army-> array that contain all characters
var army = [
  createCharacter("Claude Wallace","Edinburgh Army","Ranger Corps, Squad E","First Lieutenant","Tank Commander","Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.","http://valkyria.sega.com/img/character/chara-ss01.jpg"),
  createCharacter("Riley Miller","Edinburgh Army","Federate Joint Ops","Second Lieutenant","Artillery Advisor","Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.","http://valkyria.sega.com/img/character/chara-ss02.jpg"),
  createCharacter("Raz","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Fireteam Leader","Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.","http://valkyria.sega.com/img/character/chara-ss03.jpg"),
  createCharacter("Kai Schulen","Edinburgh Army","Ranger Corps, Squad E","Sergeant Major","Fireteam Leader","Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai.' Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault.","http://valkyria.sega.com/img/character/chara-ss04.jpg"),
  createCharacter("Minerva Victor","Edinburgh Army","Ranger Corps, Squad F","First Lieutenant","Senior Commander","Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.","http://valkyria.sega.com/img/character/chara-ss11.jpg"),
  createCharacter("Karen Stuart","Edinburgh Army","Squad E","Corporal","Combat EMT","Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.","http://valkyria.sega.com/img/character/chara-ss12.jpg"),
  createCharacter("Ragnarok","Edinburgh Army","Squad E","K-9 Unit","Mascot","Once a stray, this good good boy is lovingly referred to as 'Rags.'As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.","http://valkyria.sega.com/img/character/chara-ss13.jpg"),
  createCharacter("Miles Arbeck","Edinburgh Army","Ranger Corps, Squad E","Sergeant","Tank Operator","Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.","http://valkyria.sega.com/img/character/chara-ss15.jpg"),
  createCharacter("Dan Bentley","Edinburgh Army","Ranger Corps, Squad E","Private First Class","APC Operator","Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.","http://valkyria.sega.com/img/character/chara-ss16.jpg"),
  createCharacter("Ronald Albee","Edinburgh Army","Ranger Corps, Squad E","Second Lieutenant","Tank Operator","Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.","http://valkyria.sega.com/img/character/chara-ss17.jpg")
];
// squad-> array that contain squad character only
var squad = [];

// getting all DOM elements needed to display result
var characterContainer = document.getElementById('list');
var squadContainer = document.getElementById('squad');
var profileContainer = document.getElementById('profile');
var imageContainer = document.getElementById('image');

// creating list on body load
function createList(){
    let listHTML = "";
    for(character of army){
      listHTML += "<div onclick=\"createProfile('"+character.name+"',true)\" onmouseover=\"createProfile('"+character.name+"',false)\">";
      listHTML += character.name;
      listHTML += "</div>";
    }
    characterContainer.innerHTML=listHTML;
}

// creating character objects
function createCharacter(name,side,unit,rank,role,description,img){
  return { name:name, side:side, unit:unit, rank:rank, role:role, description:description, imageURL:img }
}

// display profile as character clicked
function createProfile(name,changeSquad){
  for(character of army){
    if(name==character.name){
      var hasSquad=false;
      for(var j=0;j<squad.length;j++){
        if(character.name==squad[j].name && changeSquad){
          hasSquad=true;
          squad.splice(j,1);
          listSquad();
        }
      }
      if(!hasSquad && changeSquad){
        if(squad.length<5){
          squad.push(createCharacter(character.name,character.side,character.unit,character.rank,character.role,character.description,character.imageURl));
        }
        listSquad();
      }
      imageContainer.innerHTML = "<img src='"+character.imageURL+"' width='600'>";
      profileContainer.innerHTML = "<ul>"+"<li>Name: "+character.name+"</li>"+"<li>Side: "+character.side+"</li>"+"<li>Unit: "+character.unit+"</li>"+"<li>Rank: "+character.rank+"</li>"+"<li>Role: "+character.role+"</li>"+"<li>Description: "+character.description+"</li>"+"</ul>";
    }
  }
  listSquad();
}

// display squad list
function listSquad(){
  var squadList ="<ul>";
  for(sqad of squad){
    squadList = squadList+ "<li onmouseover=\"createProfile('"+sqad.name+"',false)\">"+sqad.name+"</li>";
  }
  squadList = squadList+"</ul>";
  squadContainer.innerHTML = squadList;
}
